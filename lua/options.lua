require "nvchad.options"

-- add yours here!

local o = vim.o
o.tabstop = 4
o.shiftwidth = 4
o.relativenumber = true

vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
	--  group = 'userconfig',
	desc = "return cursor to where it was last time closing the file",
	pattern = "*",
	command = 'silent! normal! g`"zv',
})
vim.api.nvim_create_autocmd({ "FileType" }, {
	--  group = 'userconfig',
	desc = "Set scala indent",
	pattern = "scala",
	command = 'setlocal shiftwidth=4 softtabstop=4',
})
vim.filetype.add({
	extension = {
		snippets = "snippets",
	},
})
