return {
    ------------------------------------------------------------
    -- Disabled plugins
    ------------------------------------------------------------
    {
        "nvim-tree/nvim-tree.lua",
        enabled = false,
    },
    ------------------------------------------------------------
    -- Override config
    ------------------------------------------------------------
    {
        "stevearc/conform.nvim",
        -- event = 'BufWritePre', -- uncomment for format on save
        config = function()
            require("configs.conform")
        end,
    },
    -- These are some examples, uncomment them if you want to see them work!
    {
        "neovim/nvim-lspconfig",
        config = function()
            require("nvchad.configs.lspconfig").defaults()
            require("configs.lspconfig")
        end,
    },

    {
        "williamboman/mason.nvim",
        opts = require("configs.mason"),
    },

    {
        "nvim-treesitter/nvim-treesitter",
        dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
        },
        opts = require("configs.treesitter"),
    },

    {
        "hrsh7th/nvim-cmp",
        opts = require("configs.cmp"),
    },

    ------------------------------------------------------------
    -- Generic useful plugins for programming
    ------------------------------------------------------------

    {
        "nvim-treesitter/nvim-treesitter-textobjects",
        lazy = true,
        config = require("configs.treesitter_textobjets"),
    },

    {
        "nvim-treesitter/nvim-treesitter-context",
        ft = { "c", "cpp", "rust" },
    },

    ------------------------------------------------------------
    -- Debugging
    ------------------------------------------------------------

    {
        "folke/neoconf.nvim",
    },

    {
        "mfussenegger/nvim-dap-python",
        config = function()
            require("dap-python").setup("/usr/bin/python")
        end,
        ft = "python",
    },

    {
        "mfussenegger/nvim-dap",
        config = require("configs.dap"),
        dependencies = {
            {
                "folke/which-key.nvim",
                opts = {
                    defaults = {
                        ["<leader>d"] = { name = "+debug" },
                    },
                },
            },
        },
        --stylua: ignore
        keys = {
            { "<leader>dB", function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "Breakpoint Condition" },
            { "<leader>db", function() require("dap").toggle_breakpoint() end,                                    desc = "Toggle Breakpoint" },
            { "<leader>dc", function() require("dap").continue() end,                                             desc = "Continue" },
            { "<leader>da", function() require("dap").continue({ before = get_args }) end,                        desc = "Run with Args" },
            { "<leader>dC", function() require("dap").run_to_cursor() end,                                        desc = "Run to Cursor" },
            { "<leader>dg", function() require("dap").goto_() end,                                                desc = "Go to Line (No Execute)" },
            { "<leader>di", function() require("dap").step_into() end,                                            desc = "Step Into" },
            { "<leader>dj", function() require("dap").down() end,                                                 desc = "Down" },
            { "<leader>dk", function() require("dap").up() end,                                                   desc = "Up" },
            { "<leader>dl", function() require("dap").run_last() end,                                             desc = "Run Last" },
            { "<leader>dO", function() require("dap").step_out() end,                                             desc = "Step Out" },
            { "<leader>do", function() require("dap").step_over() end,                                            desc = "Step Over" },
            { "<leader>dp", function() require("dap").pause() end,                                                desc = "Pause" },
            { "<leader>dr", function() require("dap").repl.toggle() end,                                          desc = "Toggle REPL" },
            { "<leader>ds", function() require("dap").session() end,                                              desc = "Session" },
            { "<leader>dt", function() require("dap").terminate() end,                                            desc = "Terminate" },
            { "<leader>dw", function() require("dap.ui.widgets").hover() end,                                     desc = "Widgets" },
        },
    },

    { "nvim-neotest/nvim-nio" },
    { "folke/neodev.nvim",    opts = {} },

    {
        "rcarriga/nvim-dap-ui",
        dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" },
        keys = {
            {
                "<leader>du",
                function()
                    require("dapui").toggle({})
                end,
                desc = "Dap UI",
            },
            {
                "<leader>de",
                function()
                    require("dapui").eval()
                end,
                desc = "Dap Eval",
                mode = { "n", "v" },
            },
        },
        config = function()
            require("dapui").setup()
        end,
    },
    ------------------------------------------------------------
    -- Language specific Plugins
    ------------------------------------------------------------
    {
        "lervag/vimtex",
        ft = "tex",
        cmd = "VimtexInverseSearch",
    },

    {
        "godlygeek/tabular",
        ft = "markdown",
    },
    {
        "preservim/vim-markdown",
        ft = "markdown",
    },

    -- install with yarn or npm
    {
        "iamcco/markdown-preview.nvim",
        cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
        build = "cd app && yarn install",
        init = function()
            vim.g.mkdp_filetypes = { "markdown" }
        end,
        ft = { "markdown" },
    },

    {
        "ajorgensen/vim-markdown-toc",
        cmd = { "GenerateMarkdownTOC" },
        -- ft = { "markdown" },
    },

    {
        "https://gitlab.com/itaranto/plantuml.nvim",
        version = "*",
        ft = "puml",
        config = function()
            require("plantuml").setup({
                renderer = {
                    type = "imv",
                    options = {
                        dark_mode = true,
                    },
                },
                render_on_write = true,
            })
        end,
    },
    {
        "fatih/vim-go",
        ft = "go",
    },

    ------------------------------------------------------------
    -- Generic plugins
    ------------------------------------------------------------
    {
        "ms-jpq/chadtree",
        branch = "chad",
        build = { "python3 -m chadtree deps", "python3 -m chadtree deps" },
        cmd = "CHADopen",
    },

    {
        "tpope/vim-surround",
        lazy = false,
    },
    {
        "tpope/vim-repeat",
        lazy = false,
    },
    {
        "klen/nvim-config-local",
        lazy = false,
        config = function()
            require("config-local").setup({
                config_files = { ".nvim.lua", ".nvimrc", ".exrc" },
                hashfile = vim.fn.stdpath("data") .. "/config-local",
                autocommands_create = true,
                commands_create = true,
                silent = false,
                lookup_parents = false,
            })
        end,
    },
    {
        "sindrets/diffview.nvim",
        cmd = "DiffviewOpen",
    },

    {
        "Vonr/align.nvim",
        --stylua: ignore
        keys = {
            { "<leader>aa", function()
                local a = require("align"); a.align_to_char({ length = 1 })
            end,                                                                                                                       mode = { "x" }, desc = "Align to a character" },
            { "<leader>ad", function()
                local a = require("align"); a.align_to_char({ length = 2 })
            end,                                                                                                                       mode = { "x" }, desc = "Align to two char combo" },
            { "<leader>as", function()
                local a = require("align"); a.align_to_string({ preview = true, regex = false })
            end,                                                                                                                       mode = { "x" }, desc = "Align to string" },
            { "<leader>ar", function()
                local a = require("align"); a.align_to_string({ preview = true, regex = true })
            end,                                                                                                                       mode = { "x" }, desc = "Align to regex" },

            { "<leader>aa", function()
                local a = require("align"); a.operator(a.align_to_char, { length = 1 })
            end,                                                                                                                       mode = { "n" }, desc = "Align to a character" },
            { "<leader>ad", function()
                local a = require("align"); a.operator(a.align_to_char, { length = 2 })
            end,                                                                                                                       mode = { "n" }, desc = "Align to two char combo" },
            { "<leader>as", function()
                local a = require("align"); a.operator(a.align_to_string, { preview = true, regex = false })
            end,                                                                                                                       mode = { "n" }, desc = "Align to string" },
            { "<leader>ar", function()
                local a = require("align"); a.operator(a.align_to_string, { preview = true, regex = true })
            end,                                                                                                                       mode = { "n" }, desc = "Align to regex" },
        },
    },
}
