require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set
local del = vim.keymap.del

map("n", ";", ":", { desc = "CMD enter command mode" })
map("n", "<C-n>", "<cmd> CHADopen <CR>", { desc = "Open ChadTree" })
del("n", "<leader>e")
del("n", "<tab>")
map("n", "<C-tab>", function()
  require("nvchad.tabufline").next()
end, { desc = "goto next buffer" })

map("n", "]g", vim.diagnostic.goto_next)
map("n", "[g", vim.diagnostic.goto_prev)
map("n", "K", vim.lsp.buf.hover)
map("n", "gs", "<cmd>ClangdSwitchSourceHeader<CR>")
