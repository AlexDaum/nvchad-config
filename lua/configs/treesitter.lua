return {
	ensure_installed = {
		"vim",
		"lua",
		"html",
		"css",
		"javascript",
		"typescript",
		"tsx",
		"c",
		"cpp",
		"markdown",
		"markdown_inline",
		"go",
		"rust",
		"scala",
		"python",
        "zig",
        "vhdl"
	},
	indent = {
		enable = true,
		-- disable = {
		--   "python"
		-- },
	},
}
