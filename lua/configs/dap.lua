return function()
  -- register custom adapters
  local adapters = {"cpp"}

  for _, adapter in ipairs(adapters) do
    require("configs.dap_configs." .. adapter)
  end

  local vscode = require "dap.ext.vscode"
  local json = require "plenary.json"
  local filetypes = {
    ["debugpy"] = { "python" },
    ["gdb"] = {"c", "cpp"}
  }
  vscode.json_decode = function(str)
    return vim.json.decode(json.json_strip_comments(str))
  end
  vscode.load_launchjs(nil, filetypes)
end
