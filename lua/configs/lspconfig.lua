-- EXAMPLE 
require("nvchad.configs.lspconfig").defaults()
require("nvchad.configs.lspconfig").defaults()

local lspconfig = require "lspconfig"
local servers = { "clangd", "rust_analyzer", "cmake", "texlab", "pyright", "hls", "gopls", "metals", "vhdl_ls", "jsonls", "verible", "zls", "arduino_language_server" }
local nvlsp = require "nvchad.configs.lspconfig"

-- lsps with default config
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = nvlsp.on_attach,
    on_init = nvlsp.on_init,
    capabilities = nvlsp.capabilities,
  }
end
