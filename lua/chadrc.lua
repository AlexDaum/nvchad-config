-- This file  needs to have same structure as nvconfig.lua
-- https://github.com/NvChad/NvChad/blob/v2.5/lua/nvconfig.lua

---@type ChadrcConfig
local M = {}

M.base46 = {
    theme = "monekai",

    -- hl_override = {
    -- 	Comment = { italic = true },
    -- 	["@comment"] = { italic = true },
    -- },
    changed_themes = {
        monekai = {
            base_30 = {
                grey_fg = "#449944"
            }
        }
    }
}

M.ui = {
    statusline = {
        theme = "minimal"
    }
}

return M
